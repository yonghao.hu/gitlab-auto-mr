gitlab\_auto\_mr package
========================

Submodules
----------

gitlab\_auto\_mr.cli module
---------------------------

.. automodule:: gitlab_auto_mr.cli
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gitlab_auto_mr
    :members:
    :undoc-members:
    :show-inheritance:
